module Crud
  module Generators
    # Include this module in your generator to generate Crud views.
    # `copy_views` is the main method and by default copies all views
    # with forms.
    #
    # Copied this from the Crud views_generator.rb
    module ViewPathTemplates #:nodoc:
      extend ActiveSupport::Concern

      included do
        argument :scope, :required => false, :default => nil,
          :desc => "The scope to copy views to"

        # Le sigh, ensure Thor won't handle opts as args
        # It should be fixed in future Rails releases
        class_option :form_builder, :aliases => "-b"
        class_option :markerb

        public_task :copy_views
      end

      # TODO: Add this to Rails itslef
      module ClassMethods
        def hide!
          Rails::Generators.hide_namespace self.namespace
        end
      end

      def copy_views
        view_directory :confirmations
        view_directory :passwords
        view_directory :registrations
        view_directory :sessions
        view_directory :unlocks
      end

      protected
      def view_directory(name, _target_path = nil)
        directory name.to_s, _target_path || "#{target_path}/#{name}"
      end

      def target_path
        @target_path ||= "app/views/#{scope || :crud}"
      end
    end

    class ViewsGenerator < Rails::Generators::Base
      include ViewPathTemplates

      desc "Copies Crud views to your application."

      argument :scope, :required => false, :default => nil,
        :desc => "The scope to copy views to"

      source_root File.expand_path("../../../../app/views/crud", __FILE__)
      desc "Copies shared Crud views to your application."
      hide!

      hook_for :form_builder, :aliases => "-b",
        :desc => "Form builder to be used",
        :default => defined?(SimpleForm) ? "simple_form_for" : "form_for"
    end
  end
end
