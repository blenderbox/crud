require "rubygems"
require "country_select"
require "kaminari"
require "nested_form"
require "simple_form"
require "sass-rails"

module Crud
  class Engine < ::Rails::Engine
    if Rails.version > "3.1"
      initializer "Crud precompile hook" do |app|
        app.config.assets.paths << "#{Rails.root}/crud/assets/fonts"
        app.config.assets.precompile += %w(crud.css crud.js)
      end
    end
    config.generators do |g|
      g.test_framework :rspec, :fixture => false
      g.fixture_replacement :factory_girl, :dir => 'spec/factories'
      g.assets false
      g.helper false
    end
  end
end
