class Page < ActiveRecord::Base
  include Crud::Extensions::Slug

  attr_accessible :copy, :title, :slug

end
