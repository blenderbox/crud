module Crud
  module Extensions
    module Slug
      extend ActiveSupport::Concern

      included do
        before_validation :generate_slug

        def to_param
          self.slug
        end

        def unicode
          self.respond_to?('title') && self.title || self.name
        end

        def generate_slug
          from = self.send(self.class.slug_from)
          if from.present? && ((self.new_record? && self.slug.blank?) || (self.send("#{self.class.slug_from}_changed?") && !self.slug_changed?))
            orig_s = from.parameterize
            s = orig_s
            i = 1
            while self.class.find_by_slug(s)
              s = "#{orig_s}-#{i}"
              i += 1
            end
            self.slug = s
          end
          true
        end
      end

      module ClassMethods
        def from_param(param)
          self.find_by_slug!(param)
        end
        def slug_from
          # if something other than title or name (or if you want to skip the
          # conditional), then override this class method
          self.respond_to?('title') && 'title' || 'name'
        end
      end

    end
  end
end
