module Crud
  module Extensions
    module Status
      extend ActiveSupport::Concern

      included do
        STATUSES ||= [
          ['Active', 1,],
          ['Hidden', 0,],
          ['Inactive', -1,],
        ]

        def status_s
          STATUSES.rassoc(self.status).first rescue nil
        end
      end

      module ClassMethods
        def active
          where(:status => 1)
        end
        def hidden
          where(:status => 0)
        end
        def inactive
          where(:status => -1)
        end
      end

    end
  end
end
