class CrudController < Crud.parent_controller.constantize
  before_filter :set_obj, :only => [:new, :create,]
  before_filter :get_obj, :only => [:show, :edit, :update, :confirm_destroy, :destroy,]
  before_filter :get_collection, :only => [:index,]

  def index
    # set the 'back' session
    session[:crud_back] = request.url
  end

  def new
  end

  def create
    if @obj.save
      flash[:success] = "<strong>Success!</strong> &ldquo;#{@obj.unicode}&rdquo; has been created.".html_safe
      redirect_to_index
    else
      render :new
    end
  end

  def edit
  end

  def update
    if @obj.update(safe_params)
      flash[:success] = "<strong>Success!</strong> &ldquo;#{@obj.unicode}&rdquo; has been updated".html_safe
      redirect_to_index
    else
      render :edit
    end
  end

  def confirm_destroy
  end

  def destroy
    @obj.destroy
    flash[:success] = "<strong>Success!</strong> &ldquo;#{@obj.unicode}&rdquo; has been deleted".html_safe
    redirect_to_index
  end

  private
  def params_key
    @key ||= params[:controller].split('/').last.singularize.to_sym
  end

  def klass
    @klass ||= params[:controller].split('/').last.singularize.classify.constantize
  end

  def set_obj
    @obj = klass.new(safe_params)
  end

  def get_obj
    @obj = klass.from_param(params[:id])
  end

  def get_collection
    @collection = klass.page params[:page]
  end

  def safe_params
    # OVERRIDE ME
    # params.require(:klass).permit!
  end

  def redirect_to_index
    if session[:crud_back]
      redirect_to session[:crud_back]
    else
      redirect_to :controller => params[:controller], :action => :index
    end
  end

end
